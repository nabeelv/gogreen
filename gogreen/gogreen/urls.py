from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static

from socketio import sdjango
sdjango.autodiscover()


urlpatterns = patterns('',
    url(r'^', include('echo_server.urls')),
    url(r'^socket\.io', include(sdjango.urls)),
) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
